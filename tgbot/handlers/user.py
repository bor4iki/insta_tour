from aiogram import Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.types import Message

from tgbot.keyboards.select_city import city
from tgbot.keyboards.select_location import location
from tgbot.misc.states import Select_city


async def user_start(message: Message):
    user_name = message.from_user.full_name
    await Select_city.city.set()
    await message.answer(f"Привет {user_name}\n\n"
                         f"Давай определимся с городом:", reply_markup=city)


async def name_city(message: Message, state: FSMContext):
    text = message.text
    async with state.proxy() as data:
        data['city'] = text
    await message.answer(f"""Ты выбрал {text}
    
У нас есть 2 вида программ:

В городе: фотосессии в городе занимают от 30 минут до 3 часов

За городом: это фото-путешествие, когда мы едем за красивым контентом за город, они длятся целый день! Вся организация на нас.

Какие фотографии тебе хочется сделать?""", reply_markup=location)
    await state.finish()


def register_user(dp: Dispatcher):
    dp.register_message_handler(user_start, commands=["start"], state="*")
    dp.register_message_handler(name_city, state=Select_city)
