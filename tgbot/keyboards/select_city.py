from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

city = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Иркутск'),
            KeyboardButton(text='Москва'),
        ],
    ],
    resize_keyboard=True
)
