from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

location = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='В городе'),
            KeyboardButton(text='За городом'),
        ],
        [
            KeyboardButton(text='Посмотреть все программы'),
        ],
    ],
    resize_keyboard=True
)
